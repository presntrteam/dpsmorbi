<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'dpsmorbi');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'h<x,HW~;.X_:EY^ZCo>4V/5?f v+3^LY0DR>6X&fqL+I+[oty]`X%@3DWuSr6]gM');
define('SECURE_AUTH_KEY',  '<y6OpE;xCjSE]O(d&BZ6vz`B!w{/po)Tte|-DbrC((3~n(G/Pr&%aV)4n[onnTI?');
define('LOGGED_IN_KEY',    '!=Vk{JYvyB5l4pW6bge$[(Vt)76_Po`*gSiM{7@Lnk!yCn-dL<ZM6iZ2LzmHlA}d');
define('NONCE_KEY',        '$WIQ7Z1SR6WNou_*xWIR=9!sNU|?f,X7h1DW5rQ52TA`2qfe0<p_4hrRo7vWB;rW');
define('AUTH_SALT',        'lwH~Rx4eK~T] PXH#Zc[AR]I-],D)}LWc]9c-N+S,Sp+*67-WuCltP(M@&hR?y[O');
define('SECURE_AUTH_SALT', 'OskjX[905xnJpt%jd3FYk3^Cf}AZ)W3qm2gpyzwGRF&sieC]iXqDMpq/x>A^=]!`');
define('LOGGED_IN_SALT',   '1wsoMCAn@e-8_Vk]?v*IBZGxry/|C(^^u7|;uFlR:b/4LaWmV|}Wao@tt_sODJSw');
define('NONCE_SALT',       '8*1cCB|/eUyx .}7Wc!c-UK!aJUQ;C;p%SEB#1]o= l>~7`Z~F{B-&+GwW).hc|K');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
