<?php 
/* Template Name: Home Page */
 get_header();
$home_id = 6;
?>
<section class="about-us tc-padding-top">
	<div class="container">
		<!-- Main Heading -->
		<div class="main-heading style-2 h-white p-white">
			<h2><?php echo get_post_meta ( $home_id, 'welcome_title', true ); ?></h2>
		</div>
		<!-- About Content -->
		<div class="about-us-inner">
			<div class="row">
				<div class="col-lg-4 col-mg-5 col-sm-12">
					<div class="about-text z-depth-1">
						<?php
							$post = get_post('13'); //assuming $id has been initialized
							setup_postdata($post);
						?>
						<h3><?php the_title(); ?></h3>
						<p>
							<?php
								the_excerpt();
								wp_reset_postdata();
							?>
						</p>	
						<?php 
							$post = get_post('40');
							setup_postdata($post);
						?>
						<a class="btn blue sm" href="<?php the_permalink(); ?>">Read more<i class="fa fa-angle-right"></i></a>
						<?php wp_reset_postdata();?>
					</div>
				</div>
			<div class="col-lg-8 col-md-7 col-sm-12">
				<?php
					$post = get_post('13'); //assuming $id has been initialized
					setup_postdata($post);
				?>
					<div class="about-vid
					eo z-depth-1">
						<?php the_post_thumbnail(); ?>
					</div>
					<?php wp_reset_postdata();?>
			</div>
			</div>
		</div>
		<!-- About Content -->
		</div>
			
</section>
						
<section class="start-date">
	<div class="container">
		<h1><?php echo get_post_meta ( $home_id, 'proposed_title', true ); ?></h1>
	</div>
</section>

<section class="gallery tc-padding">
<div class="container">

	  
						
		<div class="main-heading style-2">
			<h2>Gallery</h2>
		</div>

	   

	    

	    
	      <div id="demo-test-gallery" class="demo-gallery" data-pswp-uid="1">

	        <a href="<?php echo get_template_directory_uri(); ?>/images/1.jpg" data-size="1600x1068" data-med="<?php echo get_template_directory_uri(); ?>/images/1.jpg" data-med-size="1024x1024" data-author="Folkert Gorter" class="demo-gallery__img--main">
	          <img src="<?php echo get_template_directory_uri(); ?>/images/1.jpg" alt="">
	        </a>

	        <a href="<?php echo get_template_directory_uri(); ?>/images/2.jpg" data-size="1600x1068" data-med="<?php echo get_template_directory_uri(); ?>/images/2.jpg" data-med-size="1024x683" data-author="Samuel Rohl">
	          <img src="<?php echo get_template_directory_uri(); ?>/images/2.jpg" alt="">
	          
	        </a>


	        <a href="<?php echo get_template_directory_uri(); ?>/images/3.jpg" data-size="1600x1067" data-med="<?php echo get_template_directory_uri(); ?>/images/3.jpg" data-med-size="1024x683" data-author="Ales Krivec">
	          <img src="<?php echo get_template_directory_uri(); ?>/images/3.jpg" alt="">
	          
	        </a>


	        <a href="<?php echo get_template_directory_uri(); ?>/images/4.jpg" data-size="1600x1067" data-med="<?php echo get_template_directory_uri(); ?>/images/4.jpg" data-med-size="1024x683" data-author="Michael Hull">
	          <img src="<?php echo get_template_directory_uri(); ?>/images/4.jpg" alt="">
	          
	        </a>

	        <a href="<?php echo get_template_directory_uri(); ?>/images/7.jpg" data-size="1600x1067" data-med="<?php echo get_template_directory_uri(); ?>/images/7.jpg" data-med-size="1024x683" data-author="Thomas Lefebvre">
	          <img src="<?php echo get_template_directory_uri(); ?>/images/7.jpg" alt="">
	          
	        </a>
	         <a href="<?php echo get_template_directory_uri(); ?>/images/8.jpg" data-size="1600x1067" data-med="<?php echo get_template_directory_uri(); ?>/images/8.jpg" data-med-size="1024x683" data-author="Thomas Lefebvre">
	          <img src="<?php echo get_template_directory_uri(); ?>/images/8.jpg" alt="">
	          
	        </a>
	         <a href="<?php echo get_template_directory_uri(); ?>/images/9.jpg" data-size="1600x1067" data-med="<?php echo get_template_directory_uri(); ?>/images/9.jpg" data-med-size="1024x683" data-author="Thomas Lefebvre">
	          <img src="<?php echo get_template_directory_uri(); ?>/images/9.jpg" alt="">
	          
	        </a>
	         <a href="<?php echo get_template_directory_uri(); ?>/images/10.jpg" data-size="1600x1067" data-med="<?php echo get_template_directory_uri(); ?>/images/10.jpg" data-med-size="1024x683" data-author="Thomas Lefebvre">
	          <img src="<?php echo get_template_directory_uri(); ?>/images/10.jpg" alt="">
	          
	        </a>
	         <a href="<?php echo get_template_directory_uri(); ?>/images/11.jpg" data-size="1600x1067" data-med="<?php echo get_template_directory_uri(); ?>/images/11.jpg" data-med-size="1024x683" data-author="Thomas Lefebvre">
	          <img src="<?php echo get_template_directory_uri(); ?>/images/11.jpg" alt="">
	          
	        </a>
	         <a href="<?php echo get_template_directory_uri(); ?>/images/12.jpg" data-size="1600x1067" data-med="<?php echo get_template_directory_uri(); ?>/images/12.jpg" data-med-size="1024x683" data-author="Thomas Lefebvre">
	          <img src="<?php echo get_template_directory_uri(); ?>/images/12.jpg" alt="">
	          
	        </a>



	      </div>
	   

	</div>
    
    <div id="gallery" class="pswp" tabindex="-1" role="dialog" aria-hidden="true" style="">
        <div class="pswp__bg"></div>

        <div class="pswp__scroll-wrap">

          <div class="pswp__container" style="transform: translate3d(0px, 0px, 0px);">
			<div class="pswp__item" style="display: block; transform: translate3d(-1693px, 0px, 0px);"><div class="pswp__zoom-wrap" style="transform: translate3d(297px, 44px, 0px) scale(1);"><img class="pswp__img" src="./PhotoSwipe_ Responsive JavaScript Image Gallery_files/15008465772_d50c8f0531_h.jpg" style="opacity: 1; width: 918px; height: 612px;"></div></div>
			<div class="pswp__item" style="transform: translate3d(0px, 0px, 0px);"><div class="pswp__zoom-wrap" style="transform: translate3d(386px, 239px, 0px) scale(0.379747);"><img class="pswp__img pswp__img--placeholder" src="./PhotoSwipe_ Responsive JavaScript Image Gallery_files/15008518202_b016d7d289_m.jpg" style="width: 632px; height: 632px; display: none;"><img class="pswp__img" src="./PhotoSwipe_ Responsive JavaScript Image Gallery_files/15008518202_c265dfa55f_h.jpg" style="display: block; width: 632px; height: 632px;"></div></div>
			<div class="pswp__item" style="display: block; transform: translate3d(1693px, 0px, 0px);"><div class="pswp__zoom-wrap" style="transform: translate3d(298px, 44px, 0px) scale(1);"><img class="pswp__img" src="./PhotoSwipe_ Responsive JavaScript Image Gallery_files/15008867125_b61960af01_h.jpg" style="opacity: 1; width: 917px; height: 612px;"></div></div>
          </div>

          <div class="pswp__ui pswp__ui--fit pswp__ui--hidden">

            <div class="pswp__top-bar">

				<div class="pswp__counter">1 / 5</div>

				<button class="pswp__button pswp__button--close" title="Close (Esc)"></button>

				<button class="pswp__button pswp__button--share" title="Share"></button>

				<button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button>

				<button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>

				<div class="pswp__preloader">
					<div class="pswp__preloader__icn">
					  <div class="pswp__preloader__cut">
					    <div class="pswp__preloader__donut"></div>
					  </div>
					</div>
				</div>
            </div>

            

            <button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)"></button>
            <button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)"></button>
            <div class="pswp__caption">
              <div class="pswp__caption__center">This is dummy caption.<br><small>Photo: Folkert Gorter</small></div>
            </div>
          </div>

        </div>


    </div>
    
</section>
		
<section class="comming-events tc-padding white-bg">
				<div class="container">
					
					<!-- Main Heading -->
					<div class="main-heading style-2 add-p">
						<h2><?php echo get_post_meta ( $home_id, 'contact_form_title', true ); ?></h2>
						<p><?php echo get_post_meta ( $home_id, 'contect_form_text', true ); ?></p>
					</div>
					<!-- Main Heading -->

					<!-- Form -->
					<form id="contact-form" class="row">
						<div class="col-sm-4 col-xs-4 r-full-width">
							<div class="form-group">
						      	<input required="required" type="text">
						      	<label class="control-label">Name</label><i class="bar"></i>
						    </div>
					    </div>
					    <div class="col-sm-4 col-xs-4 r-full-width">
							<div class="form-group">
						      	<input required="required" type="text">
						      	<label class="control-label">Email *</label><i class="bar"></i>
						    </div>
					    </div>
					    <div class="col-sm-4 col-xs-4 r-full-width">
							<div class="form-group">
						      	<input required="required" type="text">
						      	<label class="control-label">Phone</label><i class="bar"></i>
						    </div>
					    </div>
					    <div class="col-sm-12 col-xs-12 r-full-width">
						    <div class="form-group m-0">
						      	<textarea required="required"></textarea>
						      	<label class="control-label">Message *</label><i class="bar"></i>
						    </div>
					    </div>
					    <div class="col-sm-12 col-xs-12 r-full-width">
					    	<button class="btn blue z-depth-1">Send Message<i class="fa fa-send"></i></button>
						</div>
					</form>	
					<?php// echo do_shortcode('[contact-form-7 id="18" title="Contact Us"]');?>
					<!-- Form -->

				</div>
				
			</section>	

<?php get_footer(); ?>
