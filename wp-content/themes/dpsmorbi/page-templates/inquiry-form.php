<?php 
/* Template Name: Inquiry Form */
 get_header();

?>
<link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/js/parsleyjs/src/parsley.css">
<link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/css/bootstrap-datepicker.standalone.css">

	<?php 
		
		if(!empty($_POST)){
			global $wpdb;
			$student_name = $_POST['student_name'];
			$date_of_birth = $_POST['date_of_birth'];
			$father_name = $_POST['father_name'];
			$father_profession = $_POST['father_profession'];
			$mother_name = $_POST['mother_name'];
			$mother_profession = $_POST['mother_profession'];
			$gender = $_POST['gender'];
			$place_of_birth = $_POST['place_of_birth'];
			$class_for_admission = $_POST['class_for_admission'];
			$residential_address = $_POST['residential_address'];
			$telephone_number = $_POST['telephone_number'];
			$email = $_POST['email'];
			$current_class = $_POST['current_class'];
			$medium_of_instruction = $_POST['medium_of_instruction'];
			$skills_intrest = $_POST['skills_intrest'];
			$transport_required = $_POST['transport_required'];
			
			$tablename='wp_inquiry_form';

			$data =array('student_name' => $student_name,'date_of_birth' => $date_of_birth,'father_name' => $father_name,'father_profession' => $father_profession,'mother_name' => $mother_name,'mother_profession' => $mother_profession,'gender' => $gender,'place_of_birth' => $place_of_birth,'class_for_admission' => $class_for_admission,'residential_address' => $residential_address,'telephone_number' => $telephone_number,'email' => $email,'current_class' => $current_class,'medium_of_instruction' => $medium_of_instruction,'skills_intrest' => $skills_intrest,'transport_required' => $transport_required);
				
			$success=$wpdb->insert( $tablename, $data);	
			
			/*Mail
			$headers = "From: $student_name <$email>\r\n";
			$email_message .= "Student's Name : ".$student_name."\n";
			$email_message .= "Date Of Birth : ".$date_of_birth."\n";
			$email_message .= "Father's Name : ".$father_name."\n";
			$email_message .= "Father's Profession : ".$father_profession."\n";
			$email_message .= "Mother's Name : ".$mother_name."\n";
			$email_message .= "Mother's Profession : ".$mother_profession."\n";
			$email_message .= "Gender : ".$gender."\n";
			$email_message .= "Place Of Birth (With Taluka & Dist.) : ".$place_of_birth."\n";
			$email_message .= "Class for which admission sought : ".$class_for_admission."\n";
			$email_message .= "Residential Address : ".$residential_address."\n";
			$email_message .= "Telephone No(s) : ".$telephone_number."\n";
			$email_message .= "Email : ".$email."\n";
			$email_message .= "Class in which the child is presently studying : ".$current_class."\n";
			$email_message .= "Medium of Instruction : ".$medium_of_instruction."\n";
			$email_message .= "Special skills and Interests : ".$skills_intrest."\n";
			$email_message .= "Transport Required : ".$transport_required."\n";
			
			wp_mail( 'presntr2@gmail.com', 'Inquiry From Website', $email_message, $headers);*/
		}
		
	?>
	<section class="start-date">
		<div class="container">
			<h1><?php the_title(); ?></h1>
		</div>
	</section>
	<section class="comming-events tc-padding white-bg">
		<div class="container">
			<!---<div id="messages" class="hide" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <div id="messages_content"></div>
            </div>-->
	
			<form id="inquiry_form" method="post" action="<?php home_url( '/inquiry-form' ); ?>" data-parsley-validate novalidate>
				<div class="form-group row">
				  <label for="example-text-input" class="col-2 col-form-label">Student's Name</label>
				  <div class="col-10">
					<input class="form-control" type="text" name="student_name" required>
				  </div>
				</div>
				<div class="form-group row">
				  <label for="example-text-input" class="col-2 col-form-label">Date Of Birth</label>
				  <div class="col-10">
					<input class="form-control datepicker" type="text" name="date_of_birth">
				  </div>
				</div>
				<div class="form-group row">
				  <label for="example-text-input" class="col-2 col-form-label">Father's Name</label>
				  <div class="col-10">
					<input class="form-control" type="text" name="father_name">
				  </div>
				</div>
				<div class="form-group row">
				  <label for="example-text-input" class="col-2 col-form-label">Father's Profession</label>
				  <div class="col-10">
					<input class="form-control" type="text" name="father_profession">
				  </div>
				</div>
				<div class="form-group row">
				  <label for="example-text-input" class="col-2 col-form-label">Mother's Name</label>
				  <div class="col-10">
					<input class="form-control" type="text" name="mother_name">
				  </div>
				</div>
				<div class="form-group row">
				  <label for="example-text-input" class="col-2 col-form-label">Mother's Profession</label>
				  <div class="col-10">
					<input class="form-control" type="text" name="mother_profession">
				  </div>
				</div>
				<div class="form-group row">
				  <label for="example-text-input" class="col-2 col-form-label">Gender</label>
				  <input class="" type="radio" name="gender" value="Male" id="male"/>&nbsp;&nbsp;<label for="male" style="display: inline-block; ">Male</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				  <input class="" type="radio" name="gender" value="Female" id="female" />&nbsp;&nbsp;<label for="female" style="display: inline-block; ">Female</label>
				</div>
				<div class="form-group row">
				  <label for="example-text-input" class="col-2 col-form-label">Place Of Birth (With Taluka & Dist.)</label>
				  <div class="col-10">
					<input class="form-control" type="text" name="place_of_birth">
				  </div>
				</div>
				<div class="form-group row">
				  <label for="example-text-input" class="col-2 col-form-label">Class for which admission sought</label>
				  <div class="col-10">
					<input class="form-control" type="text" name="class_for_admission">
				  </div>
				</div>
				<div class="form-group row">
				  <label for="example-text-input" class="col-2 col-form-label">Residential Address</label>
				  <div class="col-10">
					<textarea class="form-control" name="residential_address"></textarea>
				  </div>
				</div>
				<div class="form-group row">
				  <label for="example-text-input" class="col-2 col-form-label">Telephone No(s)</label>
				  <div class="col-10">
					<input class="form-control" type="number" name="telephone_number" required>
				  </div>
				</div>
				<div class="form-group row">
				  <label for="example-text-input" class="col-2 col-form-label">Email</label>
				  <div class="col-10">
					<input class="form-control" type="email" name="email" required>
				  </div>
				</div>
				<div class="form-group row">
				  <label for="example-text-input" class="col-2 col-form-label">Class in which the child is presently studying</label>
				  <div class="col-10">
					<input class="form-control" type="text" name="current_class">
				  </div>
				</div>
				<div class="form-group row">
				  <label for="example-text-input" class="col-2 col-form-label">Medium of Instruction</label>
				  <div class="col-10">
					<input class="form-control" type="text" name="medium_of_instruction">
				  </div>
				</div>
				<div class="form-group row">
				  <label for="example-text-input" class="col-2 col-form-label">Special skills and Interests</label>
				  <div class="col-10">
					<input class="form-control" type="text" name="skills_intrest">
				  </div>
				</div>
				<div class="form-group row">
				  <label for="example-text-input" class="col-2 col-form-label">Transport Required</label>
				  <div class="col-10">
					<input class="form-control" type="text" name="transport_required">
				  </div>
				</div>
				<div class="form-group row text-center">
				  <div class="col-6">
					<button class="btn btn-primary btn-sx" name="submit" value="submit">Send Message</button>
					<button class="btn btn-warning btn-sx" name="reset" type="reset" value="reset">Cancel</button>
				  </div>
				</div>
			</form>
		</div>
	</section>
	<script src="<?php echo get_template_directory_uri();?>/js/bootstrap-datepicker.js"></script>
	<script src="<?php echo get_template_directory_uri();?>/js/parsleyjs/dist/parsley.min.js"></script>
	<script>
		(function(jQuery){
			jQuery(function(){
			jQuery('form').parsley();
			});
		})(jQuery);
		
		jQuery('.datepicker').datepicker({
			format: 'dd/mm/yyyy',
			orientation: 'auto bottom'
			
		});
		
	</script>
	<!-- <script>
            jQuery('#inquiry_form').submit(function(e) {
               jQuery('#messages').removeClass('hide').addClass('alert alert-success alert-dismissible').slideDown().show();
                jQuery('#messages_content').html('<h4>Inquiry Submited Successfully..</h4>');
                jQuery('#modal').modal('show');
                e.preventDefault();
            });
        </script>-->

<?php get_footer();?>