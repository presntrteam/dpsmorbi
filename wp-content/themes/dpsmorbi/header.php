<?php
/**
 * The Header template for our theme
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */
?><!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) & !(IE 8)]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?php wp_title( '|', true, 'right' ); ?></title>
<link rel="shortcut icon" href="#" type="image/x-icon">
<link rel="icon" href="images/favicon.png" type="image/x-icon">
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<?php // Loads HTML5 JavaScript file to add support for HTML5 elements in older IE versions. ?>
<!--[if lt IE 9]>
<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js" type="text/javascript"></script>
<![endif]-->
<?php wp_head(); ?>
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/bootstrap.min.css">
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/font-awesome.css">
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/animate.css">
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/main.css">
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/style.css">
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/color.css">
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/responsive.css">
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/transition.css">
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/site.css">
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/photoswipe.css">
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/default-skin.css">
<link href="https://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic" rel="stylesheet" type="text/css">
<script src="<?php echo get_template_directory_uri(); ?>/js/modernizr.js" style=""></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/watch.js" style=""></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/photoswipe.min.js" style=""></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/analytics.js" style=""></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/photoswipe-ui-default.min.js" style=""></script>
</head>

<body <?php body_class(); ?>>
<!-- Wrapper -->
	<div class="wrapper push-wrapper">
	<!-- Header -->
	<header>
		<!-- Top Bar -->
		<div class="top-bar">
			<div class="container">
				
				<!-- Address List -->
				<div class="address-list-top col-md-8 col-xs-6">
					<ul>
						<li><i class="fa fa-phone" aria-hidden="true"></i>+91 93740 75198</li>
						<li>+91 98250 77142</li>
						<li><i class="fa fa-envelope"></i><a href="mailto:info@dpsworldmorbi.org">info@dpsworldmorbi.org</a></li>
					</ul>
				</div>
				<!-- Address List -->
			<div class="social-top col-md-4 col-xs-6">	
				<ul class="social-icons">
					<li><a class="fa fa-twitter" href="#"></a></li>
					<li><a class="fa fa-facebook" href="#"></a></li>
					<li><a class="fa fa-instagram" href="#"></a></li>
					<li><a class="fa fa-google-plus" href="#"></a></li>
					<li><a class="fa fa-dribbble" href="#"></a></li>
				</ul>

			</div>
		</div>
		</div>
		<!-- Top Bar -->

		<!-- Nav -->
		<div class="nav-holder z-depth-1">
			<div class="container">
				
				<!-- Logo -->
				<div class="logo">
				<img class="dps_lg" src="<?php echo get_template_directory_uri(); ?>/images/DPS-World-Foundation-Logo.jpg">
					<a  href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a>
				</div>
				<!-- Logo -->
				
				<!-- Navigation -->
				<div class="nav-list">
					<?php wp_nav_menu( array( 'theme_location' => 'primary', 'container' => 'ul', 'menu_class' => '' ) ); ?>
				</div>
				<!--<div class="nav-list">
					<ul>
						<li class="active"><a href="index.html">Home</a></li>
						<li><a href="about-us.html">About Us</a></li>
						<li><a href="#">Contact Us</a></li>
					</ul>
				</div>-->
				<div class="responsive-btn">
					<a class="menu-link circle-btn" href="#menu">
					<i class="fa fa-bars" aria-hidden="true"></i>
					<i class="fa fa-close" aria-hidden="true"></i>
					</a>
				</div>
				<!-- Navigation -->

				

			</div>
		</div>
		<!-- Nav -->
	<?php if(is_front_page() && ! is_paged()):?>
		<!-- Main Slider -->
		<div id="main-slider" class="carousel slide carousel-fade">

			<!-- Wrapper for slides -->
			<div class="carousel-inner" role="listbox">
				<?php $slider_inc = 1;
					$args = array( 'post_type' => 'home_page_slider');
					$loop = new WP_Query( $args );
					while ( $loop->have_posts() ) : $loop->the_post();
					$slider_id = get_the_ID();
				?>
				<!-- Item 1 -->
				<div class="item <?php if($slider_inc == 1){ echo 'active'; } ?>">
					<?php the_post_thumbnail();?>
					<div class="container">
						<div class="caption position-center-center text-center h-white p-white">
							<!--<h1 class="font-playfair animated fadeInUp delay-1">School will be affiliate to CBSE</h1>
							<p class="animated fadeInUp delay-2"></p>-->
						</div>
					</div>
				</div> 
				<!-- Item 1 -->
				<?php 
					$slider_inc++;
					endwhile;
				?>
			</div>
			<!-- Wrapper for slides -->

			<!-- Nan Control -->
			<a class="slider-nav prev" href="#main-slider" data-slide="prev"><i class="fa fa-long-arrow-left"></i></a>
			<a class="slider-nav next" href="#main-slider" data-slide="next"><i class="fa fa-long-arrow-right"></i></a>
			<!-- Nan Control -->

			<!-- Indicators -->
			<ul class="carousel-indicators">
				<li class="active"data-target="#main-slider" data-slide-to="0"></li>
				<li data-target="#main-slider" data-slide-to="1"></li>
				<li data-target="#main-slider" data-slide-to="2"></li>
				<li data-target="#main-slider" data-slide-to="3"></li>
			</ul>
			<!-- Indicators -->

		</div>
		<!-- Main Slider -->
	<?php endif;?>
	</header>
	<!-- Header -->

	<main class="main-content">
