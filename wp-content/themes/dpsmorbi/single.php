<?php
/**
 * The Template for displaying all single posts
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */

get_header(); ?>

	<?php while ( have_posts() ) : the_post(); ?>
		<section class="start-date">
			<div class="container">
				<h1><?php the_title(); ?></h1>
			</div>
		</section>
		<section class="comming-events tc-padding white-bg">
			<div class="container">
				<?php get_template_part( 'content', get_post_format() ); ?>
			</div>
		</section>
	<?php endwhile; // end of the loop. ?>
			
<?php get_footer(); ?>
